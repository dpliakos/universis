import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgArrayPipesModule } from 'ngx-pipes';
import {environment} from '../../environments/environment';
import { StatisticsRoutingModule } from './statistics.routing';
import { HomeComponent } from './components/home/home.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SectionsComponent } from './components/sections/sections.component';
import { StatisticsService } from './services/statistics-service/statistics.service';
import { ReportsSharedModule } from './../reports-shared/reports-shared.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    StatisticsRoutingModule,
    FormsModule,
    NgArrayPipesModule,
    ReportsSharedModule
  ],
  declarations: [HomeComponent, SectionsComponent],
  providers: [
    StatisticsService
  ]
})
export class StatisticsModule {

  constructor(private readonly _translateService: TranslateService) {
    this.ngOnInit();
  }

  // TODO: it should not use ngoninit
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/statistics.${language}.json`)
        .then((translations) => {
          this._translateService.setTranslation(language, translations, true);
        })
      .catch((err) => {
        console.error(err);
      });
    });
  }
}
