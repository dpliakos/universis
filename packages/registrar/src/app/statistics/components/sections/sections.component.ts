import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ErrorService, LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';
import { ReportService } from '../../../reports-shared/services/report.service';
import { StatisticsService } from '../../services/statistics-service/statistics.service';

@Component({
  selector: 'app-statistics-sections',
  templateUrl: './sections.component.html'
})
export class SectionsComponent implements OnInit, OnDestroy {

  public category: string;
  public reportCategories: any[];
  public reportTemplates: any[];
  public search: string = null;
  private _activatedRouteSubscription: Subscription;

  constructor(
    private readonly _statisticsService: StatisticsService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _loadingService: LoadingService,
    private readonly _errorService: ErrorService,
    private readonly _reportsService: ReportService
  ) { }

  async ngOnInit() {
    this._activatedRouteSubscription = this._activatedRoute.params.subscribe(async data => {
      await this.handleRouterChange(data);
    });
  }

  async handleRouterChange(data): Promise<void> {
    if (data && data.category) {
      this.category = data.category;
    }
    await this.fetchData();
  }

  async fetchData(): Promise<void> {
    this.reportTemplates = undefined;
    try {
      this._loadingService.showLoading();
      this.reportTemplates = await this._statisticsService.getReportTemplatesOfCategory(this.category);
      console.log('reportTemplates: ', this.reportTemplates);
    } catch (err) {
      this._errorService.setLastError(err);
      this._errorService.showError(err);
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  async handleReportPrint(id: number) {
    try {
      console.debug("Attempt to print report with id ", id);
      this._reportsService.printReport(id, {

      });
    } catch(err) {
      console.error(err);
      this._errorService.showError(err);
    }
  }

  ngOnDestroy() {
    if (this._activatedRouteSubscription && !this._activatedRouteSubscription.closed) {
      this._activatedRouteSubscription.unsubscribe();
    };
  }
}
