import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
// tslint:disable-next-line:max-line-length
import {ThesesDefaultTableConfigurationResolver, ThesesTableConfigurationResolver, ThesesTableSearchResolver} from './components/theses-table/theses-table-config.resolver';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardStudentsConfigurationResolver} from './components/theses-dashboard/theses-dashboard-students/theses-dashboard-students.resolver';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardDefaultStudentsConfigurationResolver} from './components/theses-dashboard/theses-dashboard-students/theses-dashboard-students.resolver';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
  ],
  exports: [
  ],
  providers: [

    ThesesDefaultTableConfigurationResolver,
    ThesesTableConfigurationResolver,
    ThesesTableSearchResolver,
    ThesesDashboardStudentsConfigurationResolver,
    ThesesDashboardDefaultStudentsConfigurationResolver
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ThesesSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading instructors shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/theses.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}
