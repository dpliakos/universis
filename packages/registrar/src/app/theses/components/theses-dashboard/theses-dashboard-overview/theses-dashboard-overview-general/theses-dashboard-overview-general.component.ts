import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-theses-dashboard-overview-general',
  templateUrl: './theses-dashboard-overview-general.component.html',
})
export class ThesesDashboardOverviewGeneralComponent implements OnInit, OnDestroy {

  public thesis: any;
  public thesisId: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {}

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.thesis = await this._context.model('Theses')
        .where('id').equal(params.id)
        .expand('instructor,type,status,students($expand=student($expand=department,person))')
        .getItem();
      this.thesisId = params.id;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
