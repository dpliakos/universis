import {InjectionToken} from '@angular/core';

export interface RuleConfiguration {
  types: DataModelRule[];
}

export interface DataModelRule {
  entitySet: string;
  navigationProperty: string;
  children?: string[];
}

export const RULES_CONFIG = new InjectionToken<RuleConfiguration>('rules.config');
// tslint:disable quotemark
export const DEFAULT_RULES_CONFIG: RuleConfiguration = {
  types: [
    {
      'entitySet': 'CourseClasses',
      'navigationProperty': 'RegistrationRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'RegisteredCourseRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'ThesisRule',
        'InternshipRule'
      ]
    },
    {
      'entitySet': 'CourseClasses',
      'navigationProperty': 'SectionRegistrationRules',
      'children': [
        'StudentRule'
      ]
    },
    {
      'entitySet': 'StudyProgramSpecialties',
      'navigationProperty': 'GraduationRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'ThesisRule',
        'InternshipRule'
      ]
    },
    {
      'entitySet': 'StudyPrograms',
      'navigationProperty': 'InscriptionRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'ThesisRule',
        'InternshipRule'
      ]
    },
    {
      'entitySet': 'StudyPrograms',
      'navigationProperty': 'InternshipRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'ThesisRule'
      ]
    },
    {
      'entitySet': 'StudyPrograms',
      'navigationProperty': 'ThesisRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'InternshipRule'
      ]
    },
    {
      'entitySet': 'StudyProgramSpecialties',
      'navigationProperty': 'SpecialtyRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'ThesisRule',
        'InternshipRule'
      ]
    },
    {
      'entitySet': 'ProgramCourses',
      'navigationProperty': 'RegistrationRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'ThesisRule',
        'InternshipRule'
      ]
    },
    {
      'entitySet': 'Scholarships',
      'navigationProperty': 'ScholarshipRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'ThesisRule',
        'InternshipRule',
        'YearMeanGradeRule',
        'MeanGradeRule'
      ]
    }
  ]
};
// tslint:enable quotemark


