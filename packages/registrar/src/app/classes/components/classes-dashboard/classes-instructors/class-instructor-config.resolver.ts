import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.interfaces';

export class ClassInstructorsConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./classes-instructors.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./classes-instructors.config.list.json`);
        });
    }
}

export class ClassInstructorsSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./classes-instructors.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./classes-instructors.search.list.json`);
            });
    }
}

export class ClassDefaultInstructorsConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./classes-instructors.config.list.json`);
    }
}
