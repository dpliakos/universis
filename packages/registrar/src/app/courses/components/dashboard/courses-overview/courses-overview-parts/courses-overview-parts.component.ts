import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-overview-parts',
  templateUrl: './courses-overview-parts.component.html',
  styleUrls: ['./courses-overview-parts.component.scss']
})
export class CoursesOverviePartsComponent implements OnInit, OnDestroy {
  @Input() currentYear: any;
  public courseId: any;
  public model: any;
  public parts: any;
  private subscription: Subscription;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
}


  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.parts = await this._context.model('Courses')
        .where('parentCourse').equal(this._activatedRoute.snapshot.params.id)
        .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory') .getItems();

      this.courseId = params.id;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
