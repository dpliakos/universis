import {Component, Input, OnInit} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {ErrorService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import {
    AdvancedTableModalBaseComponent,
    AdvancedTableModalBaseTemplate
} from '../../../tables/components/advanced-table-modal/advanced-table-modal-base.component';
import {AdvancedFilterValueProvider} from '../../../tables/components/advanced-table/advanced-filter-value-provider.service';
import * as DEFAULT_SELECTABLE_COURSES_LIST from '../courses-table/courses-table.config.selectableCourse.json';
import {AdvancedTableConfiguration} from '../../../tables/components/advanced-table/advanced-table.component';

@Component({
    selector: 'app-select-course',
    template: AdvancedTableModalBaseTemplate
})
export class SelectCourseComponent extends AdvancedTableModalBaseComponent implements OnInit {

    @Input() tableConfig
    constructor(_router: Router, _activatedRoute: ActivatedRoute,
                _context: AngularDataContext,
                private _errorService: ErrorService,
                private _toastService: ToastService,
                private _translateService: TranslateService,
                protected advancedFilterValueProvider: AdvancedFilterValueProvider,
                protected datePipe: DatePipe) {
        super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
    }

    ngOnInit(tableConfig = DEFAULT_SELECTABLE_COURSES_LIST) {
      if(!this.tableConfig) {
        this.tableConfig = AdvancedTableConfiguration.cast(tableConfig || DEFAULT_SELECTABLE_COURSES_LIST);
      }
      super.ngOnInit();
    }

    hasInputs(): Array<string> {
        return [];
    }

    ok(): Promise<any> {
        // get selected items
        const selected = this.advancedTable.selected;
        return this.close();
    }
}
