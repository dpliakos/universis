import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as INSTRUCTORS_LIST_CONFIG from '../instructors-table/instructors-table.config.list.json';
import { TableConfiguration } from '../../../tables/components/advanced-table/advanced-table.interfaces';
import { cloneDeep } from 'lodash';
import { TemplatePipe } from '@universis/common';

@Component({
  selector: 'app-instructors-root',
  templateUrl: './instructors-root.component.html',
  styleUrls: ['./instructors-root.component.scss']
})
export class InstructorsRootComponent implements OnInit {
  public model: any;
  public instructorId: any;
  public config: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext, private _template: TemplatePipe) {
  }

  async ngOnInit() {
    this.instructorId = this._activatedRoute.snapshot.params.id;
    this.model = await this._context.model('Instructors')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department')
      .getItem();

    // @ts-ignore
    this.config = cloneDeep(INSTRUCTORS_LIST_CONFIG as TableConfiguration);

    if (this.config.columns && this.model) {
      // get actions from config file
      this.actions = this.config.columns.filter(x => {
        return x.actions;
      })
        // map actions
        .map(x => x.actions)
        // get list items
        .reduce((a, b) => b, 0);

      // filter actions with student permissions
      this.allowedActions = this.actions.filter(x => {
        if (x.role) {
          if (x.role === 'action') {
            return x;
          }
        }
      });

      this.edit = this.actions.find(x => {
        if (x.role === 'edit') {
          x.href = this._template.transform(x.href, this.model);
          return x;
        }
      });

      this.actions = this.allowedActions;
      this.actions.forEach(action => {
        action.href = this._template.transform(action.href, this.model);
      });

    }


  }
}
