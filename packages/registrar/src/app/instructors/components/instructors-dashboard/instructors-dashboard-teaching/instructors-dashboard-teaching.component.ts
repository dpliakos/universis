import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';


@Component({
  selector: 'app-instructors-dashboard-teaching',
  templateUrl: './instructors-dashboard-teaching.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardTeachingComponent implements OnInit {

public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Instructors')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department($expand=organization),user')
      .getItem();


  }
}
