import {el} from './forms.el';
import {en} from './forms.en';

export const FORMS_LOCALES = {
    el: el,
    en: en
};
