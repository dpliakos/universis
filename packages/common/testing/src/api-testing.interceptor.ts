import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TestRequest} from '@angular/common/http/testing';
import {ApiTestingController} from './api-testing-controller.service';

export class ApiTestingInterceptor implements HttpInterceptor {

    constructor(private httpController: ApiTestingController) {
        //
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const requestMapper = this.httpController.find(req);
        if (requestMapper == null) {
            return next.handle(req);
        }
        return new Observable((observer) => {
            // create an instance of test request
            const testReq = new TestRequest(req, observer);
            // call request mapper
            requestMapper.call(null, testReq);
            // finalize
            return observer.next({ type: HttpEventType.Sent });
        });
    }
}
