import {AfterViewInit, Component, ElementRef, Input, OnInit} from '@angular/core';
import 'jquery';
import * as Toast from 'bootstrap/js/dist/toast';
/**
 *
 * A native spinner component
 * @export
 * @class SpinnerComponent
 * @implements {OnInit}
 */
@Component({
    selector: 'universis-toast.toast',
    template: `
                <div class="toast-header d-flex p-0">
                  <strong class="mr-auto">{{ title }}</strong>
                  <button type="button" class="ml-2 mb-1 align-self-start close" data-dismiss="toast" (click)="hide();" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                 <div class="toast-body p-0">
                  <div class="toast-body-content">{{ message }}</div>
                </div>
                <div class="toast-header p-0">
                  <small class="toast-date">{{ dateCreated | date: 'shortTime'}}</small>
                </div>
  `,
    styles: [
        `:host {
            z-index: auto;
        }`
    ]
})


export class ToastComponent implements OnInit, AfterViewInit {

    @Input() title: string;
    @Input() message: string;
    @Input() autoHide = true;
    @Input() delay = 5000;
    public dateCreated = new Date();

    private toast: any;

    constructor(private _element: ElementRef) { }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
      this.toast = new Toast(this._element.nativeElement, {
        animation: false,
        autohide: this.autoHide,
        delay: this.delay
      });
      return this.toast.show();
    }

    public show() {
        if (this.toast) {
            this.toast.show();
        }
    }

    public hide() {
      if (this.toast) {
        this.toast.hide();
      }

      const container = document.body.getElementsByClassName('universis-toast-container')[0];
      if (container.getElementsByClassName('show').length === 1) {
        container.classList.add('hide-this-please');
      }
    }
}
