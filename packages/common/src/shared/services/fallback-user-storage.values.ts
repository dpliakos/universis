import {UserStorageInterface} from './fallback-user-storage.service';

export const SESSION_VALUE: UserStorageInterface = {
  localStorage: false,
  sessionStorage: true
};

export const LOCAL_VALUE: UserStorageInterface = {
  localStorage: true,
  sessionStorage: false
};

export const BOTH_VALUE: UserStorageInterface = {
  localStorage: true,
  sessionStorage: true
};

export const NONE_VALUE: UserStorageInterface = {
  localStorage: false,
  sessionStorage: false
};


