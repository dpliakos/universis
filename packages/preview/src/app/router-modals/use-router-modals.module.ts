import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormioModule } from 'angular-formio';
import {RouterModalSimpleComponent} from './router-modal-simple.component';
import {OkCancelActionComponent} from './ok-cancel-action.component';
import {YesNoComponent} from './yes-no.component';
import {ModalEditComponent} from './modal-edit-form.component';
import {EditForm1Component} from './edit-form1.component';
import {RouterModalModule} from '@universis/common/routing';
import {UseRouterModalsRoutingModule} from './use-router-modals.routing';
import {FormsModule} from '@angular/forms';
import {HighlightIncludeModule} from '../highlight/highlight.module';
import {TabsModule} from 'ngx-bootstrap';
import {RouterModalAdvancedComponent} from './router-modal-advanced.component';
import {AbortRetryIgnoreComponent} from './abort-retry-ignore.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FormioModule,
        RouterModalModule,
        HighlightIncludeModule,
        UseRouterModalsRoutingModule,
        TabsModule
    ],
    declarations: [
        RouterModalSimpleComponent,
        RouterModalAdvancedComponent,
        OkCancelActionComponent,
        YesNoComponent,
        AbortRetryIgnoreComponent,
        ModalEditComponent,
        EditForm1Component
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UseRouterModalsModule { }
